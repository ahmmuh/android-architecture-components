package se.makesite.androidarchitecturecomponents

import android.arch.lifecycle.MediatorLiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel

class MainViewModel : ViewModel() {

    val fullName = MutableLiveData<String>()

}